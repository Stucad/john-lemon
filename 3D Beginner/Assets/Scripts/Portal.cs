using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public Transform  Tp;
    public GameObject player;


    private void OnTriggerEnter(Collider other)
    {
        if(TimePortales.instace.cantUsed)
        {
          player.transform.position = Tp.transform.position;
          TimePortales.instace.OnTp();
        }
    }
}

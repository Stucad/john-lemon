using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimePortales : MonoBehaviour
{
    public static TimePortales instace;
    public float myTime;
    float lastTime;
    public bool cantUsed;
    private void Awake()
    {
        if(instace == null)
        {
            instace = this;
        }
        else
        {
            Destroy(this);
        }
    }
    private void Update()
    {
        if(lastTime > myTime)
        {
            cantUsed = true;
            return;
        }

        lastTime += Time.deltaTime;
    }

    public void OnTp()
    {
        cantUsed = false;
        lastTime = 0f;
    }

}
